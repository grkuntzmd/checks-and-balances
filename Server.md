# Server

## Server Operations

### Get all transactions for a particular year

```http
GET /transactions/yyyy
```

### Get the date of the oldest transaction

```http
GET /transactions/date-of-first
```

### Get balances for a particular year

```http
GET /balances/yyyy
```

### Create or update a transaction

```http
PUT /transactions
```

- Update `Dates` index (remove from old and add to new, if applicable)
- Update balances from the transaction date going forward to present

### Delete a transaction

```http
DELETE /transactions/UUID
```

- Update `Dates` index (remove)
- Update balances from the transaction date going forward to present

## Tables

### Transactions

<dl>
  <dt>Key</dt>
  <dd><tt>UUID</tt></dd>

  <dt>Value</dt>
  <dd>JSON encoding of transaction</dd>
</dl>

### Balances

<dl>
  <dt>Key</dt>
  <dd><tt>yyyy</tt></dd>

  <dt>Value</dt>
  <dd>JSON encoding of balances (actual, bank)</dd>
</dl>

### Dates

<dl>
<dt>Key</dt>
<dd><tt>yyyy-MM-dd</tt></dd>

<dt>Value</dt>
<dd><tt>[UUID]</tt></dd>

</dl>