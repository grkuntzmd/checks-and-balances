/*
 * Copyright © 2017, G.Ralph Kuntz, MD.
 *
 * Licensed under the Apache License, Version 2.0(the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIC
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package boltdb

import (
	"fmt"
	"time"

	"bitbucket.org/grkuntzmd/checks-and-balances/server/models"
	"github.com/coreos/bbolt"
)

// ReadBalances reads the balances record for the given year.
func (r *Repository) ReadBalances(date time.Time) (*models.Balances, error) {
	var balances models.Balances

	return &balances, r.DB.View(func(tx *bolt.Tx) error {
		balancesBucket := tx.Bucket(balancesBucketName)
		if balancesBucket == nil {
			return fmt.Errorf("%s bucket not found", balancesBucketName)
		}

		g := balancesBucket.Get([]byte(date.Format(models.DateFormat)))
		if g == nil {
			return nil
		}

		return unmarshal(g, balances)
	})
}
