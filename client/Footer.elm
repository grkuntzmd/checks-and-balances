module Footer exposing (view)

import Date.Extra exposing (toFormattedString)
import Decimal
import Html exposing (Html, button, div, form, i, input, label, text)
import Html.Attributes
    exposing
        ( checked
        , class
        , for
        , id
        , type_
        , value
        )
import Html.Events exposing (onClick, onInput)
import Maybe exposing (map, withDefault)
import Model exposing (Model)
import Msg exposing (Msg(..))
import Transaction exposing (TransactionKind(..), TransactionType(..))


view : Model -> Html Msg
view model =
    form [ class "footer w3-padding w3-theme" ]
        [ details model
        , kind model
        , amount model
        , add model
        ]


details : Model -> Html Msg
details model =
    let
        checkNumber =
            map (toString) model.transactionCheckNumber
                |> withDefault ""

        date =
            map (toFormattedString "yyyy-MM-dd") model.transactionDate
                |> withDefault ""

        transactionDateColor =
            if model.transactionDateRequired then
                " error"
            else
                ""

        transactionDescriptionColor =
            if model.transactionDescriptionRequired then
                " error"
            else
                ""
    in
        div [ class "center-flex-row" ]
            [ div [ class "col" ]
                [ label [ for "check-number" ] [ text "Check Number (Optional)" ]
                , input
                    [ class "narrow w3-input w3-margin-left w3-round-large"
                    , id "check-number"
                    , onInput TransactionCheckNumber
                    , type_ "number"
                    , value checkNumber
                    ]
                    []
                ]
            , div [ class "col" ]
                [ label [ for "transaction-date" ] [ text "Transaction Date" ]
                , input
                    [ "w3-input w3-margin-left w3-round-large"
                        ++ transactionDateColor
                        |> class
                    , id "transaction-date"
                    , onInput TransactionDate
                    , value date
                    , type_ "date"
                    ]
                    []
                ]
            , div [ class "col" ]
                [ label [ for "description" ] [ text "Description" ]
                , input
                    [ "w3-input w3-margin-left w3-round-large"
                        ++ transactionDescriptionColor
                        |> class
                    , id "description"
                    , onInput TransactionDescription
                    , type_ "text"
                    , model.transactionDescription |> value
                    ]
                    []
                ]
            ]


kind : Model -> Html Msg
kind model =
    div [ class "start-flex-column" ]
        [ div [ class "baseline-flex-row col" ]
            [ input
                [ model.transactionType == WithdrawalType |> checked
                , class "w3-radio"
                , id "withdrawal"
                , SetTransactionType WithdrawalType |> onClick
                , type_ "radio"
                ]
                []
            , label [ class "w3-margin-left", for "withdrawal" ]
                [ text "Withdrawal" ]
            ]
        , div [ class "baseline-flex-row col" ]
            [ input
                [ model.transactionType == DepositType |> checked
                , class "w3-radio"
                , id "deposit"
                , SetTransactionType DepositType |> onClick
                , type_ "radio"
                ]
                []
            , label [ class "w3-margin-left", for "deposit" ]
                [ text "Deposit" ]
            ]
        ]


amount : Model -> Html Msg
amount model =
    let
        amount_ =
            withDefault "" model.transactionAmount

        transactionAmountColor =
            if model.transactionAmountRequired then
                " error"
            else
                ""
    in
        div [ class "center-flex-row" ]
            [ div [ class "col" ]
                [ label [ for "amount" ] [ text "Amount" ]
                , input
                    [ "w3-input w3-margin-left w3-round-large"
                        ++ transactionAmountColor
                        |> class
                    , id "amount"
                    , onInput TransactionAmount
                    , type_ "text"
                    , value amount_
                    ]
                    []
                ]
            ]


add : Model -> Html Msg
add model =
    let
        addButtonText =
            case model.transactionUuid of
                Just _ ->
                    "Update"

                Nothing ->
                    "Add"

        updateButton =
            button
                [ class "w3-border w3-btn w3-ripple w3-small"
                , onClick TransactionAdd
                ]
                [ text addButtonText ]

        buttons =
            case model.transactionUuid of
                Just _ ->
                    [ button
                        [ class "w3-border w3-btn w3-margin-right w3-ripple w3-small"
                        , onClick CancelEdit
                        ]
                        [ text "Cancel" ]
                    , updateButton
                    ]

                Nothing ->
                    [ updateButton ]
    in
        div [ class "center-flex-row" ] buttons
