/*
 * Copyright © 2017, G.Ralph Kuntz, MD.
 *
 * Licensed under the Apache License, Version 2.0(the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIC
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/grkuntzmd/checks-and-balances/server/models"
	"github.com/gorilla/mux"
)

func deleteTransaction(rep models.Repository) http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		vars := mux.Vars(req)
		uuid := vars["uuid"]

		if err := rep.DeleteTransaction([]byte(uuid)); err != nil {
			http.Error(res, fmt.Sprintf("cannot delete transactions: %s", err), http.StatusInternalServerError)
			return
		}

		res.WriteHeader(http.StatusNoContent)
	}
}

func getDateOfFirstTransaction(rep models.Repository) http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		date, err := rep.ReadDateOfFirstTransaction()
		if err != nil {
			http.Error(res, fmt.Sprintf("cannot get date of first transaction: %s", err), http.StatusInternalServerError)
			return
		}

		j, err := json.Marshal(date)
		if err != nil {
			http.Error(res, fmt.Sprintf("cannot convert date to JSON: %s", err), http.StatusInternalServerError)
		}

		res.Header().Set("Content-Type", "application/json; charset=UTF-8")
		res.WriteHeader(http.StatusOK)
		if _, err = res.Write(j); err != nil {
			panic(err)
		}
	}
}

func getTransactions(rep models.Repository) http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		vars := mux.Vars(req)
		year := vars["year"]

		startDate, err := time.Parse(models.DateFormat, year+"-01-01")
		if err != nil {
			http.Error(res, fmt.Sprintf("cannot parse \"from\" date: %s", err), http.StatusBadRequest)
			return
		}

		transactions, err := rep.ReadTransactions(startDate)
		if err != nil {
			http.Error(res, fmt.Sprintf("cannot read transactions: %s", err), http.StatusInternalServerError)
			return
		}

		j, err := json.Marshal(transactions)
		if err != nil {
			http.Error(res, fmt.Sprintf("cannot convert transactions to JSON: %s", err), http.StatusInternalServerError)
		}

		res.Header().Set("Content-Type", "application/json; charset=UTF-8")
		res.WriteHeader(http.StatusOK)
		if _, err = res.Write(j); err != nil {
			panic(err)
		}
	}
}

func putTransaction(rep models.Repository) http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		t := new(models.Transaction)
		err := json.NewDecoder(req.Body).Decode(t)
		if err != nil {
			http.Error(res, fmt.Sprintf("decoding JSON body: %v", err), http.StatusBadRequest)
			return
		}

		if err := rep.WriteTransaction(t); err != nil {
			http.Error(res, fmt.Sprintf("cannot write transactions: %s", err), http.StatusInternalServerError)
			return
		}

		res.WriteHeader(http.StatusNoContent)
	}
}
