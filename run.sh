#!/bin/sh

cd "${0%/*}"

webpack

if go-bindata -debug -o server/controllers/bindata.go -pkg controllers -dev ./dist
then
    buildInfo="`date -u '+%Y-%m-%dT%TZ'`|`git describe --always --long`|`git tag | tail -1`"
    go run -ldflags "-X main.buildInfo=${buildInfo} -s -w" ./cmd/checks-and-balances/main.go "$@"
fi