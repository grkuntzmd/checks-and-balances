module Nbsp exposing (nbsp)

import Exts.Html as Ext
import String exposing (join, words)


nbsp : String -> String
nbsp string =
    words string |> join Ext.nbsp
