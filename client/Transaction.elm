module Transaction
    exposing
        ( Balances
        , Transaction
        , TransactionKind(..)
        , TransactionType(..)
        )

import Date exposing (Date)
import Decimal exposing (Decimal)
import Uuid exposing (Uuid)


type alias Balances =
    { actualBalance : Decimal
    , bankBalance : Decimal
    }


type alias Transaction =
    { actualBalance : Decimal
    , bankBalance : Decimal
    , checkNumber : Maybe Int
    , date : Date
    , description : String
    , kind : TransactionKind
    , reconciled : Bool
    , uuid : Uuid
    }


type TransactionKind
    = Deposit Decimal
    | Withdrawal Decimal


type TransactionType
    = DepositType
    | WithdrawalType
