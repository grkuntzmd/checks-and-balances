/*
 * Copyright © 2017, G.Ralph Kuntz, MD.
 *
 * Licensed under the Apache License, Version 2.0(the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIC
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package boltdb

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"io"
	"log"
	"math/rand"
	"os"
	"os/signal"
	"path"
	"syscall"
	"time"

	"bitbucket.org/grkuntzmd/checks-and-balances/server/decimal"
	"bitbucket.org/grkuntzmd/checks-and-balances/server/models"
	"github.com/coreos/bbolt"
	"github.com/google/uuid"
	"github.com/tebeka/atexit"
)

// Repository contains the boltdb connection.
type Repository struct {
	*bolt.DB
}

var (
	balancesBucketName     = []byte("Balances")
	datesBucketName        = []byte("Dates")
	transactionsBucketName = []byte("Transactions")
)

// New creates and returns a new repository.
func New(databaseDirectory string) (*Repository, error) {
	// var isNew bool
	// if _, err := os.Stat(databaseDirectory); err == nil {
	// 	isNew = false
	// } else if os.IsNotExist(err) {
	// 	isNew = true
	// } else {
	// 	return nil, err
	// }

	db, err := bolt.Open(path.Join(databaseDirectory, "checks-and-balances.db"), 0600, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		return nil, err
	}

	atexit.Register(gracefulShutdown(db))

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		switch signal := <-interrupt; signal {
		case syscall.SIGINT:
			gracefulShutdown(db)
		case syscall.SIGTERM:
			gracefulShutdown(db)
		}
		atexit.Exit(0)
	}()

	rep := &Repository{db}

	if err := rep.createBuckets(); err != nil {
		return nil, err
	}

	// if isNew {
	// 	if err := rep.createFakeData(); err != nil {
	// 		return nil, err
	// 	}
	// }

	return rep, nil
}

func (r *Repository) createBuckets() error {
	return r.DB.Update(func(tx *bolt.Tx) error {
		if _, err := tx.CreateBucketIfNotExists(balancesBucketName); err != nil {
			return err
		}

		if _, err := tx.CreateBucketIfNotExists(datesBucketName); err != nil {
			return err
		}

		if _, err := tx.CreateBucketIfNotExists(transactionsBucketName); err != nil {
			return err
		}

		return nil
	})
}

func firstDateOfYear(date time.Time) time.Time {
	return time.Date(date.Year(), time.January, 1, 0, 0, 0, 0, time.Local)
}

func gracefulShutdown(db io.Closer) func() {
	return func() {
		log.Println("Closing DB")
		_ = db.Close()
	}
}

func marshal(v interface{}) ([]byte, error) {
	var buf bytes.Buffer
	if err := gob.NewEncoder(&buf).Encode(v); err != nil {
		return nil, err
	}
	// if err := json.NewEncoder(&buf).Encode(v); err != nil {
	// 	return nil, err
	// }

	return buf.Bytes(), nil
}

func unmarshal(data []byte, v interface{}) error {
	buf := bytes.NewBuffer(data)
	return gob.NewDecoder(buf).Decode(v)
	// return json.NewDecoder(buf).Decode(v)
}

// ***********************************************************
// Fake data creation
// ***********************************************************

func (r *Repository) createFakeData() error {
	startYear := 2017
	month := time.Month(rand.Int31n(12) + 1)
	day := int(rand.Int31n(31) + 1)

	oldest := time.Date(startYear, month, day, 0, 0, 0, 0, time.Local)

	transactions := []*models.Transaction{
		&models.Transaction{
			UUID:        uuid.New().String(),
			CheckNumber: nil,
			Date:        oldest,
			Description: "Initial balance",
			Amount:      decimal.NewFromInt(10000),
			Reconciled:  true,
		},
	}

	rand.Seed(time.Now().Unix())
	checkNumber := models.CheckNumber(100)
	deposits := []string{
		"Lottery winnings",
		"Paycheck",
		"Social security check",
		"Tax refund",
	}
	withdrawals := []string{
		"Amazon",
		"Beer",
		"Computer supplies",
		"Electric",
		"Ice cream",
		"Karate",
		"Publix",
		"Water",
	}

	for i := 0; i < 200; i++ {
		deposit := rand.Int31n(20) < 1
		var date time.Time
		for {
			var year int
			if startYear < 2017 {
				year = int(int32(startYear) + rand.Int31n(int32(2017-startYear)))
			} else {
				year = startYear
			}

			month := time.Month(rand.Int31n(12) + 1)
			day := int(rand.Int31n(31) + 1)
			date = time.Date(year, month, day, 0, 0, 0, 0, time.Local)
			if date.After(oldest) && date.Before(time.Now()) {
				break
			}
		}
		var number *models.CheckNumber
		if !deposit {
			checkNumber++
			c := checkNumber
			number = &c
		}
		var description string
		if deposit {
			description = deposits[rand.Int31n(int32(len(deposits)))]
		} else {
			description = withdrawals[rand.Int31n(int32(len(withdrawals)))]
		}
		var amount decimal.Decimal
		if deposit {
			amount = decimal.NewFromInt((rand.Int31n(5) + 1) * 50)
		} else {
			amount = decimal.New(-(rand.Int31n(5000) + 1), -2)
		}
		var reconciled bool
		if date.Year() == 2017 {
			reconciled = rand.Int31n(2) < 1
		} else {
			reconciled = true
		}

		transactions = append(transactions, &models.Transaction{
			UUID:        uuid.New().String(),
			CheckNumber: number,
			Date:        date,
			Description: description,
			Amount:      amount,
			Reconciled:  reconciled,
		})
	}

	for _, t := range transactions {
		if err := r.WriteTransaction(t); err != nil {
			return err
		}
	}

	for _, t := range transactions {
		if err := r.WriteTransaction(t); err != nil {
			return err
		}
	}

	return r.DB.View(func(tx *bolt.Tx) error {
		return printDatabase(tx)
	})
}

func printDatabase(tx *bolt.Tx) error {
	if err := printBalances(tx); err != nil {
		return err
	}

	if err := printDates(tx); err != nil {
		return err
	}

	return printTransactions(tx)
}

func printBalances(tx *bolt.Tx) error {
	log.Print(string(balancesBucketName))
	b := tx.Bucket(balancesBucketName)
	if b == nil {
		return fmt.Errorf("%s bucket not found", balancesBucketName)
	}

	return b.ForEach(func(k, v []byte) error {
		balances := new(models.Balances)
		if err := unmarshal(v, balances); err != nil {
			return err
		}

		log.Printf("\t%s; %+v", string(k), *balances)

		return nil
	})
}

func printDates(tx *bolt.Tx) error {
	log.Print(string(datesBucketName))
	b := tx.Bucket(datesBucketName)
	if b == nil {
		return fmt.Errorf("%s bucket not found", datesBucketName)
	}

	return b.ForEach(func(k, v []byte) error {
		dates := new(models.Dates)
		if err := unmarshal(v, dates); err != nil {
			return err
		}

		log.Printf("\t%s: %+v", string(k), *dates)

		return nil
	})
}

func printTransactions(tx *bolt.Tx) error {
	log.Print(string(transactionsBucketName))
	return traverseTransactionsForDates(time.Unix(0, 0), time.Now(), tx, func(t *models.Transaction, _ *bolt.Bucket, _ *bolt.Tx) error {
		log.Printf("\t%s; %+v", t.UUID, *t)
		return nil
	})
}
