/*
 * Copyright © 2017, G.Ralph Kuntz, MD.
 *
 * Licensed under the Apache License, Version 2.0(the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIC
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package boltdb

import (
	"fmt"
	"time"

	"bitbucket.org/grkuntzmd/checks-and-balances/server/models"
	"github.com/coreos/bbolt"
)

// DeleteTransaction deletes the given transaction and updates the Dates index.
func (r *Repository) DeleteTransaction(uuid []byte) error {
	return r.DB.Update(func(tx *bolt.Tx) error {
		startBalanceDate := time.Now()

		b := tx.Bucket(transactionsBucketName)
		if b == nil {
			return fmt.Errorf("%s bucket not found", transactionsBucketName)
		}

		g := b.Get(uuid)
		if g != nil {
			transaction := new(models.Transaction)
			if err := unmarshal(g, transaction); err != nil {
				return err
			}

			if err := removeUUIDForDate(transaction.Date, transaction.UUID, tx); err != nil {
				return err
			}

			startBalanceDate = earlierDate(transaction.Date, startBalanceDate)
		}

		startBalanceDate = firstDateOfYear(startBalanceDate)

		return b.Delete(uuid)
	})
}

// ReadDateOfFirstTransaction returns the date of the oldest transaction in the database.
func (r *Repository) ReadDateOfFirstTransaction() (time.Time, error) {
	date := time.Now()

	return date, r.DB.View(func(tx *bolt.Tx) error {
		datesBucket := tx.Bucket(datesBucketName)
		if datesBucket == nil {
			return fmt.Errorf("%s bucket not found", datesBucketName)
		}

		c := datesBucket.Cursor()
		k, _ := c.First()
		if k != nil {
			d, err := time.Parse(models.DateFormat, string(k))
			if err != nil {
				return err
			}

			date = d
		}

		return nil
	})
}

// ReadTransactions reads all transaction for the given year.
func (r *Repository) ReadTransactions(fromDate time.Time) ([]*models.Transaction, error) {
	var transactions []*models.Transaction

	return transactions, r.DB.View(func(tx *bolt.Tx) error {
		var endDate = fromDate.AddDate(1, 0, 0).AddDate(0, 0, -1)

		return traverseTransactionsForDates(fromDate, endDate, tx, func(t *models.Transaction, _ *bolt.Bucket, _ *bolt.Tx) error {
			transactions = append(transactions, t)
			return nil
		})
	})
}

// WriteTransaction creates or updates the given transaction and updates the Dates index and Balances table.
func (r *Repository) WriteTransaction(t *models.Transaction) error {
	return r.DB.Update(func(tx *bolt.Tx) error {
		startBalanceDate, err := updateTransaction(t, tx)
		if err != nil {
			return err
		}

		if err = updateDates(t, tx); err != nil {
			return err
		}

		return updateBalances(t, startBalanceDate, tx)
	})
}

func findBalancesBeforeTransaction(t *models.Transaction, startBalanceDate time.Time, tx *bolt.Tx) (balancesDate time.Time, balances models.Balances, err error) {
	balancesBucket := tx.Bucket(balancesBucketName)
	if balancesBucket == nil {
		err = fmt.Errorf("%s bucket not found", balancesBucketName)
		return
	}

	found := false
	c := balancesBucket.Cursor()

	for k, v := c.First(); k != nil; k, v = c.Next() {
		var date time.Time
		date, err = time.Parse(models.DateFormat, string(k))
		if err != nil {
			return
		}

		if date.After(t.Date) {
			break
		}

		found = true
		balancesDate = date

		if err = unmarshal(v, &balances); err != nil {
			return
		}
	}

	// If no balance was found, write a 0/0 balance for the year of this transaction.
	if !found {
		balancesDate = firstDateOfYear(t.Date)
		err = writeBalances(balancesDate, balances, tx, balancesBucket)
	}

	return
}

func updateBalances(t *models.Transaction, startBalanceDate time.Time, tx *bolt.Tx) error {
	transactionsBucket := tx.Bucket(transactionsBucketName)
	if transactionsBucket == nil {
		return fmt.Errorf("%s bucket not found", transactionsBucketName)
	}

	balancesDate, balances, err := findBalancesBeforeTransaction(t, startBalanceDate, tx)
	if err != nil {
		return err
	}

	return traverseTransactionsForDates(balancesDate, time.Now(), tx, func(t *models.Transaction, b *bolt.Bucket, tx *bolt.Tx) error {
		if err := writeBalancesForNewYear(&balancesDate, balances, t.Date, tx); err != nil {
			return err
		}

		balances.ActualBalance = balances.ActualBalance.Add(t.Amount)
		if t.Reconciled {
			balances.BankBalance = balances.BankBalance.Add(t.Amount)
		}

		return nil
	})
}

func updateDates(t *models.Transaction, tx *bolt.Tx) error {
	b := tx.Bucket(datesBucketName)
	if b == nil {
		return fmt.Errorf("%s bucket not found", datesBucketName)
	}

	date := []byte(t.Date.Format(models.DateFormat))
	var dates models.Dates
	g := b.Get(date)
	if g != nil {
		if err := unmarshal(g, &dates); err != nil {
			return err
		}
	} else {
		dates = models.Dates{}
	}

	found := false
	for _, u := range dates {
		if u == t.UUID {
			found = true
			break
		}
	}

	if !found {
		dates = append(dates, t.UUID)
		g, err := marshal(dates)
		if err != nil {
			return err
		}

		return b.Put(date, g)
	}

	return nil
}

func updateTransaction(t *models.Transaction, tx *bolt.Tx) (startBalanceDate time.Time, err error) {
	startBalanceDate = t.Date
	uuid := []byte(t.UUID)

	b := tx.Bucket(transactionsBucketName)
	if b == nil {
		err = fmt.Errorf("%s bucket not found", transactionsBucketName)
		return
	}

	g := b.Get(uuid)
	if g != nil {
		transaction := new(models.Transaction)
		if err = unmarshal(g, transaction); err != nil {
			return
		}

		if transaction.Date != t.Date {
			if err = removeUUIDForDate(transaction.Date, transaction.UUID, tx); err != nil {
				return
			}
		}

		startBalanceDate = earlierDate(transaction.Date, startBalanceDate)
	}

	startBalanceDate = firstDateOfYear(startBalanceDate)

	g, err = marshal(t)
	if err != nil {
		return
	}

	err = b.Put(uuid, g)
	return
}

func writeBalancesForNewYear(balancesDate *time.Time, balances models.Balances, date time.Time, tx *bolt.Tx) error {
	// If we have advanced the year, write the balance for the first of the year.
	if balancesDate.Year() != date.Year() {
		*balancesDate = firstDateOfYear(date)
		balancesBucket := tx.Bucket(balancesBucketName)
		if balancesBucket == nil {
			return fmt.Errorf("%s bucket not found", balancesBucketName)
		}

		key := []byte(balancesDate.Format(models.DateFormat))
		g, err := marshal(balances)
		if err != nil {
			return err
		}

		if err = balancesBucket.Put(key, g); err != nil {
			return err
		}
	}

	return nil
}

func writeBalances(date time.Time, balances models.Balances, tx *bolt.Tx, balancesBucket *bolt.Bucket) error {
	var g []byte
	g, err := marshal(balances)
	if err != nil {
		return err
	}

	return balancesBucket.Put([]byte(date.Format(models.DateFormat)), g)
}
