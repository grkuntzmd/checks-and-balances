/*
 * Copyright © 2017, G.Ralph Kuntz, MD.
 *
 * Licensed under the Apache License, Version 2.0(the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIC
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package boltdb

import (
	"bytes"
	"fmt"
	"time"

	"bitbucket.org/grkuntzmd/checks-and-balances/server/models"
	"github.com/coreos/bbolt"
)

func earlierDate(date1, date2 time.Time) time.Time {
	if date1.Before(date2) {
		return date1
	}

	return date2
}

func removeUUIDForDate(date time.Time, uuid string, tx *bolt.Tx) error {
	datesBucket := tx.Bucket(datesBucketName)
	if datesBucket == nil {
		return fmt.Errorf("%s bucket not found", datesBucketName)
	}

	d := []byte(date.Format(models.DateFormat))
	g := datesBucket.Get(d)
	if g == nil {
		return fmt.Errorf("uuid %s not found for date %s", uuid, date.Format(models.DateFormat))
	}

	var dates models.Dates
	if err := unmarshal(g, &dates); err != nil {
		return err
	}

	uuids := dates[:0]
	for _, u := range dates {
		if u != uuid {
			uuids = append(uuids, u)
		}
	}
	dates = uuids

	g, err := marshal(dates)
	if err != nil {
		return err
	}

	return datesBucket.Put(d, g)
}

func traverseDates(fromDate time.Time, toDate time.Time, tx *bolt.Tx, fn func(date time.Time, dates models.Dates, tx *bolt.Tx) error) error {
	datesBucket := tx.Bucket(datesBucketName)
	if datesBucket == nil {
		return fmt.Errorf("%s bucket not found", datesBucketName)
	}

	from, to := []byte(fromDate.Format(models.DateFormat)), []byte(toDate.Format(models.DateFormat))
	c := datesBucket.Cursor()

	for k, v := c.Seek(from); k != nil && bytes.Compare(k, to) <= 0; k, v = c.Next() {
		date, err := time.Parse(models.DateFormat, string(k))
		if err != nil {
			return err
		}

		var dates models.Dates
		if err = unmarshal(v, &dates); err != nil {
			return err
		}

		if err = fn(date, dates, tx); err != nil {
			return err
		}
	}

	return nil
}

func traverseTransactionsForDates(fromDate time.Time, toDate time.Time, tx *bolt.Tx, fn func(t *models.Transaction, b *bolt.Bucket, tx *bolt.Tx) error) error {
	transactionsBucket := tx.Bucket(transactionsBucketName)
	if transactionsBucket == nil {
		return fmt.Errorf("%s bucket not found", transactionsBucketName)
	}

	return traverseDates(fromDate, toDate, tx, func(date time.Time, dates models.Dates, tx *bolt.Tx) error {
		for _, uuid := range dates {
			g := transactionsBucket.Get([]byte(uuid))
			if g == nil {
				return fmt.Errorf("cannot read transaction with uuid %s", uuid)
			}

			transaction := new(models.Transaction)
			if err := unmarshal(g, transaction); err != nil {
				return err
			}

			if err := fn(transaction, transactionsBucket, tx); err != nil {
				return err
			}
		}

		return nil
	})
}
