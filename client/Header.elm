module Header exposing (licenseID, view)

import Date.Extra exposing (Interval, range, toFormattedString)
import Date.Extra as DExtra
import Html
    exposing
        ( Attribute
        , Html
        , a
        , button
        , div
        , h2
        , h3
        , h5
        , h6
        , header
        , i
        , img
        , input
        , label
        , option
        , p
        , select
        , text
        )
import Html.Attributes
    exposing
        ( checked
        , class
        , for
        , height
        , href
        , id
        , placeholder
        , selected
        , src
        , target
        , type_
        , value
        , width
        )
import Html.Events exposing (on, onClick, onInput)
import Json.Decode as Json
import List.Extra as LExtra
import Maybe exposing (withDefault)
import Model exposing (Model)
import Msg exposing (Msg(..))
import Nbsp exposing (nbsp)
import Status exposing (Status(..))


licenseID : String
licenseID =
    "license"


view : Model -> Html Msg
view model =
    div [ class "header" ]
        [ top model
        , bottom model
        ]


top : Model -> Html Msg
top model =
    let
        classes_ =
            if model.dialogOpen == licenseID then
                "dialog showing w3-modal"
            else
                "dialog hidden w3-modal"
    in
        div [ class "w3-padding w3-theme" ]
            [ div [ class "center-flex-row" ]
                [ img [ height 32, src "/favicon.svg", width 32 ] []
                , h3 [ class "w3-margin-left" ] [ text "Checks and Balances" ]
                ]
            , div [ class "center-flex-row" ]
                [ h5 [] [ text "Copyright © 2017, G. Ralph Kuntz, MD" ]
                , button
                    [ class "w3-border w3-btn w3-margin-left w3-padding-small w3-ripple w3-round-large w3-small"
                    , onClick ShowLicense
                    ]
                    [ text "Apache 2.0 License" ]
                ]
            , div [ class classes_, id licenseID ]
                [ div [ class "license-content w3-animate-zoom w3-card-4 w3-modal-contents" ]
                    [ header [ class "w3-container w3-theme" ]
                        [ h2 [] [ text "Apache 2.0 License" ] ]
                    , div [ class "w3-container w3-white" ]
                        [ p []
                            [ text "Copyright 2017, G. Ralph Kuntz, MD" ]
                        , p []
                            [ text "Licensed under the Apache License, Version 2.0 (the \"License\"); you may not use this file except in compliance with the License. You may obtain a copy of the License at" ]
                        , p [ class "apache-href" ]
                            [ a
                                [ class "apache-href narrow-text"
                                , href "http://www.apache.org/licenses/LICENSE-2.0"
                                , target "_blank"
                                ]
                                [ text "http://www.apache.org/licenses/LICENSE-2.0" ]
                            ]
                        , p []
                            [ text "Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License." ]
                        ]
                    ]
                ]
            ]


bottom : Model -> Html Msg
bottom model =
    div [ class "w3-padding w3-theme" ]
        [ status model
        , firstMonth model
        , search model
        ]


status : Model -> Html Msg
status model =
    div [ class "center-flex-row" ]
        [ div [ class "baseline-flex-row col" ]
            [ input
                [ model.showStatus == All |> checked
                , class "w3-radio"
                , id "all"
                , SetStatus All |> onClick
                , type_ "radio"
                ]
                []
            , label [ class "w3-margin-left", for "all" ]
                [ text "All" ]
            ]
        , div [ class "baseline-flex-row col" ]
            [ input
                [ model.showStatus == Open |> checked
                , class "w3-radio"
                , id "open"
                , SetStatus Open |> onClick
                , type_ "radio"
                ]
                []
            , label [ class "w3-margin-left", for "open" ]
                [ text "Open" ]
            ]
        , div [ class "baseline-flex-row col" ]
            [ input
                [ model.showStatus == Reconciled |> checked
                , class "w3-radio"
                , id "reconciled"
                , SetStatus Reconciled |> onClick
                , type_ "radio"
                ]
                []
            , label [ class "w3-margin-left", for "reconciled" ]
                [ text "Reconciled" ]
            ]
        ]


firstMonth : Model -> Html Msg
firstMonth model =
    let
        dateRange =
            range DExtra.Month 1 model.oldestMonth model.currentDate

        chosen =
            LExtra.find (\d -> d == model.firstMonth) dateRange
                |> Maybe.map (toFormattedString "yyyy-MM")
                |> withDefault "1970-01"

        options =
            dateRange
                |> List.map
                    (\d ->
                        let
                            tag =
                                toFormattedString "yyyy-MM" d

                            text_ =
                                toFormattedString "MMM yyyy" d
                        in
                            option
                                [ chosen == tag |> selected
                                , value tag
                                ]
                                [ text text_ ]
                    )
    in
        div [ class "center-flex-row first-month" ]
            [ h6 [] [ nbsp "First Month to Show" |> text ]
            , select
                [ class "w3-margin-left w3-round-large w3-select"
                , onChange SetFirstMonth
                , value chosen
                ]
                options
            ]


onChange : (String -> msg) -> Attribute msg
onChange handler =
    on "change" <| Json.map handler <| Json.at [ "target", "value" ] Json.string


search : Model -> Html Msg
search model =
    div [ class "center-flex-row" ]
        [ input
            [ class "w3-input w3-round-large"
            , onInput Search
            , placeholder "Search"
            , type_ "search"
            , value model.searchTerm
            ]
            []
        ]
