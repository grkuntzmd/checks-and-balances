module Model exposing (Model, epoch, init)

import Date exposing (Date, Month(..), now)
import Date.Extra exposing (fromCalendarDate)
import Decimal exposing (Decimal, zero)
import Msg exposing (Msg(..))
import Random.Pcg exposing (Seed, generate, initialSeed, int, maxInt, minInt)
import Server exposing (fetchOldestDate)
import Status exposing (Status(..))
import Task exposing (perform)
import Transaction
    exposing
        ( Transaction
        , TransactionKind(..)
        , TransactionType(..)
        )
import Uuid exposing (Uuid)


type alias Model =
    { actualBalance : Decimal
    , allTransactions : List Transaction
    , bankBalance : Decimal
    , currentDate : Date
    , delete : Maybe Uuid
    , dialogOpen : String
    , displayedTransactions : List Transaction
    , error : Maybe String
    , firstMonth : Date
    , oldestMonth : Date
    , searchTerm : String
    , seed : Seed
    , showStatus : Status
    , transactionAmount : Maybe String
    , transactionAmountRequired : Bool
    , transactionCheckNumber : Maybe Int
    , transactionDate : Maybe Date
    , transactionDateRequired : Bool
    , transactionDescription : String
    , transactionDescriptionRequired : Bool
    , transactionType : TransactionType
    , transactionUuid : Maybe Uuid
    }


epoch : Date
epoch =
    fromCalendarDate 1970 Jan 1


initialModel : Model
initialModel =
    { actualBalance = zero
    , allTransactions = []
    , bankBalance = zero
    , currentDate = epoch
    , delete = Nothing
    , dialogOpen = ""
    , displayedTransactions = []
    , error = Nothing
    , firstMonth = epoch
    , oldestMonth = epoch
    , searchTerm = ""
    , seed = initialSeed 0
    , showStatus = All
    , transactionAmount = Nothing
    , transactionAmountRequired = False
    , transactionCheckNumber = Nothing
    , transactionDate = Nothing
    , transactionDateRequired = False
    , transactionDescription = ""
    , transactionDescriptionRequired = False
    , transactionType = WithdrawalType
    , transactionUuid = Nothing
    }


init : ( Model, Cmd Msg )
init =
    ( initialModel
    , Cmd.batch
        [ Task.perform ReceiveDate now
        , generate (InitializeSeeds) (int minInt maxInt)
        , fetchOldestDate
        ]
    )
