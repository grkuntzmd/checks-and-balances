#!/bin/sh

cd "${0%/*}"

webpack

if go-bindata -o server/controllers/bindata.go -pkg controllers ./dist
then
    buildInfo="`date -u '+%Y-%m-%dT%TZ'`|`git describe --always --long`|`git tag | tail -1`"
    GOOS=windows GOARCH=amd64 go build -ldflags "-X main.buildInfo=${buildInfo} -s -w -H windowsgui" ./cmd/...
fi