module Msg exposing (Msg(..))

import Date exposing (Date)
import Dom exposing (Error)
import Http
import Random.Pcg exposing (Seed)
import Remote exposing (RemoteTransaction)
import RemoteData exposing (WebData)
import Status exposing (Status)
import Transaction exposing (Balances, Transaction, TransactionType(..))
import Time exposing (Time)
import Uuid exposing (Uuid)


type Msg
    = CancelEdit
    | ConfirmDelete Uuid
    | DeleteCancel
    | DeleteOK
    | DeleteTransaction (List Transaction) (Result Http.Error String)
    | Edit Uuid
    | FetchBalances (WebData Balances)
    | FetchOldestDate (WebData String)
    | FetchTransactions (WebData (List RemoteTransaction))
    | HideDialog
    | InitializeSeeds Int
    | NoOpDom (Result Dom.Error ())
    | NoOpRemote (Result Http.Error String)
    | PutTransaction Bool Seed (List Transaction) (Result Http.Error String)
    | ReceiveDate Date
    | Reconcile Uuid
    | Search String
    | SetFirstMonth String
    | SetStatus Status
    | SetTransactionType TransactionType
    | ShowLicense
    | Tick Time
    | TransactionAdd
    | TransactionAmount String
    | TransactionCheckNumber String
    | TransactionDate String
    | TransactionDescription String
