module Status exposing (Status(..))


type Status
    = All
    | Open
    | Reconciled
