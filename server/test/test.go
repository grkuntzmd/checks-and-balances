/*
 * Copyright © 2017, G.Ralph Kuntz, MD.
 *
 * Licensed under the Apache License, Version 2.0(the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIC
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"path/filepath"
	"reflect"
	"regexp"
	"runtime"
	"testing"
)

// Modelled after https://github.com/benbjohnson/testing.

// Assert fails the test if the condition is false.
func Assert(tb testing.TB, condition bool, msg string, v ...interface{}) {
	if !condition {
		_, file, line, _ := runtime.Caller(1)
		tb.Errorf("\033[31m%s:%d: "+msg+"\033[39m\n\n", append([]interface{}{filepath.Base(file), line}, v...)...)
	}
}

// Equals fails the test if exp is not equal to act.
func Equals(tb testing.TB, exp, act interface{}) {
	if !reflect.DeepEqual(exp, act) {
		_, file, line, _ := runtime.Caller(1)
		tb.Errorf("\033[31m%s:%d:\n\n\texp: %#v\n\n\tgot: %#v\033[39m\n\n", filepath.Base(file), line, exp, act)
		tb.FailNow()
	}
}

// ExpectedError fails the test if err is not equal to message.
func ExpectedError(tb testing.TB, message string, err error) {
	_, file, line, _ := runtime.Caller(1)
	if err == nil {
		tb.Errorf("\033[31m%s:%d:\n\n\texp: %#v\n\n\tgot: nil\033[39m\n\n", filepath.Base(file), line, message)
		tb.FailNow()
	}
	if !reflect.DeepEqual(message, err.Error()) {
		tb.Errorf("\033[31m%s:%d:\n\n\texp: %#v\n\n\tgot: %#v\033[39m\n\n", filepath.Base(file), line, message, err.Error())
	}
}

// False fails the test if value is false.
func False(tb testing.TB, act bool) {
	if act {
		_, file, line, _ := runtime.Caller(1)
		tb.Errorf("\033[31m%s:%d: value should be false\033[39m\n\n", filepath.Base(file), line)
	}
}

// Match checks that an expression matches a regexp pattern.
func Match(tb testing.TB, pat string, act interface{}) {
	_, file, line, _ := runtime.Caller(1)
	matched, err := regexp.MatchString(pat, fmt.Sprintf("%v", act))
	OK(tb, err)
	Assert(tb, matched, fmt.Sprintf("\033[31m%s:%d:\n\n\tshould match `%s`", filepath.Base(file), line, pat))
}

// NotEquals fails the test if exp is not equal to act.
func NotEquals(tb testing.TB, exp, act interface{}) {
	if reflect.DeepEqual(exp, act) {
		_, file, line, _ := runtime.Caller(1)
		tb.Errorf("\033[31m%s:%d:\n\n\tvalues should not be equal\033[39m\n\n", filepath.Base(file), line)
	}
}

// NotNil checks that a value is not nil.
func NotNil(tb testing.TB, act interface{}) {
	if act == nil {
		_, file, line, _ := runtime.Caller(1)
		tb.Errorf("\033[31m%s:%d: unexpected nil value\033[39m\n\n", filepath.Base(file), line)
	}
}

// OK fails the test if an err is not nil.
func OK(tb testing.TB, err error) {
	if err != nil {
		_, file, line, _ := runtime.Caller(1)
		tb.Errorf("\033[31m%s:%d: unexpected error: %s\033[39m\n\n", filepath.Base(file), line, err.Error())
	}
}

// StatusOK fails if the response is not OK (200). It prints the body of the response if it exists.
func StatusOK(tb testing.TB, rr *httptest.ResponseRecorder) {
	if rr.Code != http.StatusOK {
		_, file, line, _ := runtime.Caller(1)
		body := rr.Body.String()
		tb.Errorf("\033[31m%s:%d: unexpected response: %d\n\n\tbody: %s\033[39m\n\n", filepath.Base(file), line, rr.Code, body)
	}
}

// True fails the test if value is false.
func True(tb testing.TB, act bool) {
	if !act {
		_, file, line, _ := runtime.Caller(1)
		tb.Errorf("\033[31m%s:%d: value should be true\033[39m\n\n", filepath.Base(file), line)
	}
}
