"use strict";

require("font-awesome/css/font-awesome.css");
require("w3-css/w3.css");
require("./w3-theme-potters-clay.css");
require("./styles.scss");

require("./index.html");

const Elm = require("./Main.elm");
const app = Elm.Main.fullscreen();

window.onload = function() {
    var dialogs = document.getElementsByClassName("w3-modal");
    window.onclick = function(event) {
        for (var d = 0; d < dialogs.length; d++) {
            if (event.target === dialogs[d]) {
                app.ports.dialogClosed.send(null);
            }
        }
    };
};