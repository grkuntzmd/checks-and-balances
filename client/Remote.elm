module Remote exposing (RemoteTransaction)

import Decimal exposing (Decimal)


type alias RemoteTransaction =
    { checkNumber : Maybe Int
    , date : String
    , description : String
    , amount : Decimal
    , reconciled : Bool
    , uuid : String
    }
