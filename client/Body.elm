module Body exposing (confirmID, view)

import Date.Extra exposing (toFormattedString)
import Decimal exposing (lt, toNormalizedString, zero)
import Html
    exposing
        ( Html
        , button
        , div
        , h3
        , header
        , i
        , input
        , table
        , tbody
        , td
        , text
        , tfoot
        , th
        , thead
        , tr
        )
import Html.Attributes exposing (checked, class, id, type_)
import Html.Events exposing (onClick)
import Maybe exposing (withDefault)
import Model exposing (Model)
import Msg exposing (Msg(..))
import Transaction exposing (TransactionKind(..))


confirmID : String
confirmID =
    "confirm"


errorID : String
errorID =
    "error"


view : Model -> Html Msg
view model =
    let
        rows =
            List.map
                (\t ->
                    let
                        bankBalanceRed =
                            if lt t.bankBalance zero then
                                " color-red"
                            else
                                ""

                        actualBalanceRed =
                            if lt t.actualBalance zero then
                                " color-red"
                            else
                                ""

                        ( deposit, withdrawal ) =
                            case t.kind of
                                Deposit amount ->
                                    ( toNormalizedString amount, "" )

                                Withdrawal amount ->
                                    ( "", toNormalizedString amount )

                        attributes =
                            if Just t.uuid == model.transactionUuid then
                                [ class "highlight" ]
                            else
                                []
                    in
                        tr attributes
                            [ td [ class "text-align-right" ]
                                [ Maybe.map (toString) t.checkNumber
                                    |> withDefault ""
                                    |> text
                                ]
                            , td []
                                [ toFormattedString "MM/dd/yyyy" t.date |> text ]
                            , td [] [ text t.description ]
                            , td [ class "text-align-center" ]
                                [ input
                                    [ checked t.reconciled
                                    , class "w3-check"
                                    , Reconcile t.uuid |> onClick
                                    , type_ "checkbox"
                                    ]
                                    []
                                ]
                            , td [ class "text-align-right" ]
                                [ text deposit ]
                            , td [ class "text-align-right" ]
                                [ text withdrawal ]
                            , td
                                [ "text-align-right"
                                    ++ bankBalanceRed
                                    |> class
                                ]
                                [ toNormalizedString t.bankBalance
                                    |> text
                                ]
                            , td
                                [ "text-align-right"
                                    ++ actualBalanceRed
                                    |> class
                                ]
                                [ toNormalizedString t.actualBalance
                                    |> text
                                ]
                            , td
                                [ class "text-align-center" ]
                                [ i
                                    [ class "fa fa-pencil"
                                    , Edit t.uuid |> onClick
                                    ]
                                    []
                                ]
                            , td
                                [ class "text-align-center"
                                , ConfirmDelete t.uuid |> onClick
                                ]
                                [ i [ class "fa fa-trash" ] [] ]
                            ]
                )
                model.displayedTransactions

        confirmClasses_ =
            if model.dialogOpen == confirmID then
                "dialog showing w3-modal"
            else
                "dialog hidden w3-modal"

        ( errorClasses_, error ) =
            case model.error of
                Just error_ ->
                    ( "dialog showing w3-modal", error_ )

                Nothing ->
                    ( "dialog hidden w3-modal", "" )
    in
        div [ class "body", id "body" ]
            [ div [ id "container" ]
                [ table [ class "w3-bordered w3-hoverable w3-striped w3-table" ]
                    [ thead []
                        [ tr []
                            [ th [ id "check-number-col" ]
                                [ text "Chk #"
                                , div [] [ text "Chk #" ]
                                ]
                            , th [ id "date-col" ]
                                [ text "Date"
                                , div [] [ text "Date" ]
                                ]
                            , th [ id "description-col" ]
                                [ text "Description"
                                , div [] [ text "Description" ]
                                ]
                            , th [ id "reconcile-col" ]
                                [ text "Rec"
                                , div [] [ text "Rec" ]
                                ]
                            , th [ id "deposit-col" ]
                                [ text "Deposit"
                                , div [] [ text "Deposit" ]
                                ]
                            , th [ id "withdrawal-col" ]
                                [ text "Withdrawal"
                                , div [] [ text "Withdrawal" ]
                                ]
                            , th [ id "bank-balance-col" ]
                                [ text "Bank Balance"
                                , div [] [ text "Bank Balance" ]
                                ]
                            , th [ id "actual-balance-col" ]
                                [ text "Actual Balance"
                                , div [] [ text "Actual Balance" ]
                                ]
                            , th [ id "edit-col" ]
                                [ text "Edit"
                                , div [] [ text "Edit" ]
                                ]
                            , th [ id "delete-col" ]
                                [ text "Delete"
                                , div [] [ text "Delete" ]
                                ]
                            ]
                        ]
                    , tbody [] rows
                    , tfoot []
                        []
                    ]
                ]
            , div [ class confirmClasses_, id confirmID ]
                [ div [ class "confirm-content w3-animate-zoom w3-card-4 w3-modal-contents" ]
                    [ header [ class "w3-container w3-theme" ]
                        [ h3 [] [ text "Delete" ] ]
                    , div [ class "w3-container w3-white" ]
                        [ h3 [ class "text-align-center" ] [ text "Are you sure?" ] ]
                    , div [ class "w3-container w3-padding w3-theme" ]
                        [ button
                            [ class "w3-border w3-btn w3-clear w3-margin-left w3-padding-small w3-right w3-ripple w3-round-large w3-small"
                            , onClick DeleteCancel
                            ]
                            [ text "Cancel" ]
                        , button
                            [ class "w3-border w3-btn w3-clear w3-margin-left w3-padding-small w3-right w3-ripple w3-round-large w3-small"
                            , onClick DeleteOK
                            ]
                            [ text "Ok" ]
                        ]
                    ]
                ]
            , div [ class errorClasses_, id errorID ]
                [ div [ class "confirm-content w3-animate-zoom w3-card-4 w3-modal-contents" ]
                    [ header [ class "w3-container w3-theme" ]
                        [ h3 [] [ text "Server Error" ] ]
                    , div [ class "w3-container w3-white" ]
                        [ h3 [ class "text-align-center" ] [ text error ] ]
                    ]
                ]
            ]
