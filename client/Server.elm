module Server
    exposing
        ( deleteTransactionCmd
        , fetchBalances
        , fetchOldestDate
        , fetchTransactions
        , pingServerCmd
        , putTransactionCmd
        )

import Date.Extra exposing (toIsoString)
import Decimal exposing (Decimal(..))
import Http
import Json.Decode exposing (Decoder, bool, int, list, nullable, string)
import Json.Decode.Pipeline exposing (decode, optional, required)
import Json.Encode exposing (Value)
import Json.Encode as Encode
import Maybe exposing (map)
import Msg exposing (Msg(..))
import Random.Pcg exposing (Seed)
import Remote exposing (RemoteTransaction)
import RemoteData
import Transaction exposing (Balances, Transaction, TransactionKind(..))
import Uuid exposing (Uuid)


deleteTransactionCmd : List Transaction -> Uuid -> Cmd Msg
deleteTransactionCmd transactions uuid =
    deleteTransactionRequest uuid
        |> Http.send (DeleteTransaction transactions)


deleteTransactionRequest : Uuid -> Http.Request String
deleteTransactionRequest uuid =
    Http.request
        { body = Http.emptyBody
        , expect = Http.expectString
        , headers = []
        , method = "DELETE"
        , timeout = Nothing
        , url = "/transactions/" ++ (Uuid.toString uuid)
        , withCredentials = False
        }


fetchBalances : String -> Cmd Msg
fetchBalances year =
    Http.get ("/balances/" ++ year) balancesDecoder
        |> RemoteData.sendRequest
        |> Cmd.map Msg.FetchBalances


fetchOldestDate : Cmd Msg
fetchOldestDate =
    Http.get "/transactions/date-of-first" string
        |> RemoteData.sendRequest
        |> Cmd.map Msg.FetchOldestDate


fetchTransactions : Int -> Cmd Msg
fetchTransactions year =
    Http.get ("/transactions/" ++ (toString year)) (list transactionDecoder)
        |> RemoteData.sendRequest
        |> Cmd.map Msg.FetchTransactions


pingServerCmd : Cmd Msg
pingServerCmd =
    Http.send NoOpRemote pingServerRequest


pingServerRequest : Http.Request String
pingServerRequest =
    Http.request
        { body = Http.emptyBody
        , expect = Http.expectString
        , headers = []
        , method = "OPTIONS"
        , timeout = Nothing
        , url = "/"
        , withCredentials = False
        }


putTransactionCmd : Bool -> Seed -> List Transaction -> Transaction -> Cmd Msg
putTransactionCmd recalcDates seed transactions transaction =
    putTransactionRequest transaction
        |> Http.send (PutTransaction recalcDates seed transactions)


putTransactionRequest : Transaction -> Http.Request String
putTransactionRequest transaction =
    Http.request
        { body = transactionEncoder transaction |> Http.jsonBody
        , expect = Http.expectString
        , headers = [ Http.header "Content-Type" "application/json; charset=UTF-8" ]
        , method = "PUT"
        , timeout = Nothing
        , url = "/transactions"
        , withCredentials = False
        }


balancesDecoder : Decoder Balances
balancesDecoder =
    decode Balances
        |> required "actualBalance" decimalDecoder
        |> required "bankBalance" decimalDecoder


decimalDecoder : Decoder Decimal
decimalDecoder =
    decode Decimal
        |> required "m" int
        |> required "e" int


decimalEncoder : Decimal -> Value
decimalEncoder decimal =
    let
        (Decimal m e) =
            decimal

        attributes =
            [ ( "m", Encode.int m )
            , ( "e", Encode.int e )
            ]
    in
        Encode.object attributes


transactionDecoder : Decoder RemoteTransaction
transactionDecoder =
    decode RemoteTransaction
        |> optional "checkNumber" (nullable int) Nothing
        |> required "date" string
        |> required "description" string
        |> required "amount" decimalDecoder
        |> required "reconciled" bool
        |> required "uuid" string


transactionEncoder : Transaction -> Value
transactionEncoder transaction =
    let
        attributes =
            [ map (\c -> ( "checkNumber", Encode.int c ))
                transaction.checkNumber
            , Just
                ( "date"
                , toIsoString transaction.date
                    |> Encode.string
                )
            , Just ( "description", Encode.string transaction.description )
            , Just
                ( "amount"
                , case transaction.kind of
                    Deposit amount ->
                        decimalEncoder amount

                    Withdrawal amount ->
                        Decimal.negate amount |> decimalEncoder
                )
            , Just ( "reconciled", Encode.bool transaction.reconciled )
            , Just ( "uuid", Uuid.toString transaction.uuid |> Encode.string )
            ]
                |> List.filterMap identity
    in
        Encode.object attributes
