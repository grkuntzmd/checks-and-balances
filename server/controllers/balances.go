/*
 * Copyright © 2017, G.Ralph Kuntz, MD.
 *
 * Licensed under the Apache License, Version 2.0(the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIC
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"bitbucket.org/grkuntzmd/checks-and-balances/server/models"
	"github.com/gorilla/mux"
)

func getBalancesForYear(rep models.Repository) http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		vars := mux.Vars(req)
		year := vars["year"]

		date, err := time.Parse(models.DateFormat, year+"-01-01")
		if err != nil {
			http.Error(res, fmt.Sprintf("cannot parse \"from\" date: %s", err), http.StatusBadRequest)
			return
		}

		balances, err := rep.ReadBalances(date)
		if err != nil {
			http.Error(res, fmt.Sprintf("cannot read balances: %s", err), http.StatusInternalServerError)
			return
		}

		j, err := json.Marshal(balances)
		if err != nil {
			http.Error(res, fmt.Sprintf("cannot convert balances to JSON: %s", err), http.StatusInternalServerError)
		}

		res.Header().Set("Content-Type", "application/json; charset=UTF-8")
		res.WriteHeader(http.StatusOK)
		if _, err = res.Write(j); err != nil {
			panic(err)
		}
	}
}
