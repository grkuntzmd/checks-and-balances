/*
 * Copyright © 2017, G.Ralph Kuntz, MD.
 *
 * Licensed under the Apache License, Version 2.0(the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIC
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package decimal

import (
	"fmt"
	"strconv"
	"strings"
)

// Decimal represents a mantissa and exponent.
type Decimal struct {
	Mantissa int32 `json:"m"`
	Exponent int32 `json:"e"`
}

// Zero is the 0 value for Decimal.
var Zero = NewFromInt(0)

// New creates a new Decimal.
func New(m int32, e int32) Decimal {
	return Decimal{m, e}
}

// NewFromInt creates a new Decimal from an integer (exponent 0).
func NewFromInt(m int32) Decimal {
	return New(m, 0)
}

// NewFromString parses a string representation of a Decimal.
func NewFromString(s string) (Decimal, error) {
	parts := strings.Split(s, ".")

	switch len(parts) {
	case 1:
		return stringIntToDecimal(parts[0], 0)
	case 2:
		return stringIntToDecimal(parts[0]+parts[1], int32(-len(parts[1])))
	default:
		return Decimal{0, 0}, fmt.Errorf("unexpected string format \"%s\" for conversion to Decimal", s)
	}
}

// Add adds another Decimal to the current Decimal.
func (a Decimal) Add(b Decimal) Decimal {
	a, b = a.ToCommonExponent(b)
	return Decimal{a.Mantissa + b.Mantissa, a.Exponent}
}

// Compare compares another Decimal to the current Decimal.
func (a Decimal) Compare(b Decimal) int {
	a, b = a.ToCommonExponent(b)
	diff := a.Mantissa - b.Mantissa
	switch {
	case diff > 0:
		return 1
	case diff < 0:
		return -1
	default:
		return 0
	}
}

// Eq tests equality of another Decimal to the current Decimal.
func (a Decimal) Eq(b Decimal) bool {
	return a.Compare(b) == 0
}

// Lt tests that the current Decimal is less than another Decimal.
func (a Decimal) Lt(b Decimal) bool {
	return a.Compare(b) < 0
}

// Negate negates the current Decimal.
func (a Decimal) Negate() Decimal {
	return Decimal{-a.Mantissa, a.Exponent}
}

// NormalizeHundredths normalizes the exponent of the current Decimal to 2 places.
func (a Decimal) NormalizeHundredths() Decimal {
	return a.addInt(2 + a.Exponent)
}

// String converts a Decimal to a string with 2 places and satisfies the fmt.Stringer interface.
func (a Decimal) String() string {
	return a.NormalizeHundredths().toString()
}

// Sub subtracts another Decimal from the current Decimal.
func (a Decimal) Sub(b Decimal) Decimal {
	return a.Add(b.Negate())
}

// ToCommonExponent normalizes two Decimals to have the same exponent.
func (a Decimal) ToCommonExponent(b Decimal) (Decimal, Decimal) {
	var e int32
	if a.Exponent < b.Exponent {
		e = a.Exponent
	} else {
		e = b.Exponent
	}

	return a.toExponent(e), b.toExponent(e)
}

// Truncate truncates a Decimal to the given number of places.
func (a Decimal) Truncate(n int) Decimal {
	s := a.toString()

	parts := strings.Split(s, ".")
	l := len(parts)
	switch {
	case l == 1 && n >= 0:
		return toDecimal(parts[0][:n])
	case l == 1 && n < 0:
		return toDecimal(parts[0])
	case l == 2 && n >= 0:
		return toDecimal(parts[0] + "." + parts[1][:n])
	case l == 2 && n < 0:
		return toDecimal(parts[0] + "." + parts[1][:-n])
	default:
		return Zero
	}
}

func (a Decimal) addInt(n int32) Decimal {
	switch {
	case n == 0:
		return Decimal{a.Mantissa, a.Exponent}
	case n > 0:
		return (Decimal{a.Mantissa * 10, a.Exponent - 1}).addInt(n - 1)
	default:
		return a
	}
}

func stringIntToDecimal(s string, e int32) (Decimal, error) {
	m, err := strconv.Atoi(s)
	if err != nil {
		return Decimal{0, 0}, err
	}

	return Decimal{int32(m), e}, nil
}

func toDecimal(s string) Decimal {
	d, err := NewFromString(s)
	if err != nil {
		return Zero
	}
	return d
}

func (a Decimal) toExponent(e int32) Decimal {
	return a.addInt(a.Exponent - e)
}

func (a Decimal) toString() string {
	var absM int32
	var sign string
	if a.Mantissa >= 0 {
		absM = a.Mantissa
		sign = ""
	} else {
		absM = -a.Mantissa
		sign = "-"
	}

	s := strconv.Itoa(int(absM))

	switch {
	case a.Exponent > 0:
		return sign + s + strings.Repeat("0", int(a.Exponent))

	case a.Exponent < 0:
		pos := 0 - a.Exponent

		extraZeros := int(pos) - len(s)

		var paddedS string
		if extraZeros >= 0 {
			paddedS = strings.Repeat("0", extraZeros+1) + s
		} else {
			paddedS = s
		}

		before := paddedS[:len(paddedS)-int(pos)]

		after := paddedS[len(paddedS)-int(pos):]
		return sign + before + "." + after

	default:
		return sign + s
	}
}
