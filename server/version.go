/*
 * Copyright © 2017, G.Ralph Kuntz, MD.
 *
 * Licensed under the Apache License, Version 2.0(the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIC
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package server

// Info contains version information.
type Info struct {
	BuildStamp string
	GitHash    string
	Version    string
}

// NewInfo creates and returns a new information structure.
func NewInfo() Info {
	return Info{
		BuildStamp: "No BuildStamp provided",
		GitHash:    "No GitHash provided",
		Version:    "No Version provided",
	}
}
