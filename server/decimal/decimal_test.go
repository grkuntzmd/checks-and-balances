/*
 * Copyright © 2017, G.Ralph Kuntz, MD.
 *
 * Licensed under the Apache License, Version 2.0(the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIC
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package decimal_test

import (
	"testing"

	"bitbucket.org/grkuntzmd/checks-and-balances/server/decimal"
	"bitbucket.org/grkuntzmd/checks-and-balances/server/test"
)

func TestDecimal(t *testing.T) {
	t.Parallel()

	t.Run("Add should add two Decimals", testAdd)
	t.Run("Compare should compare two Equal Decimals", testCompareEqual)
	t.Run("Compare should compare two Decimals with first greater", testCompareGreater)
	t.Run("Compare should compare two Decimals with first less", testCompareLess)
	t.Run("Eq should compare two different Decimals", testEqDifferent)
	t.Run("Eq should compare two same Decimals", testEqSame)
	t.Run("Lt should compare two Decimals where first is greater", testLtFalse)
	t.Run("Lt should compare two Decimals where first is less", testLtTrue)
	t.Run("Negate should negate a Decimal", testNegate)
	t.Run("NormalizeHundredths should normalize a Decimal to 2 places", testNormalizeHundredths)
	t.Run("New should create a new Decimal", testNew)
	t.Run("NewFromInt should create a new Decimal", testNewFromInt)
	t.Run("NewFromString should create a new Decimal", testNewFromString)
	t.Run("NewFromString should return an error", testNewFromStringInvalid)
	t.Run("Sub should subtract two Decimals", testSub)
	t.Run("ToCommonExponent should find the common exponent between two Decimals", testToCommonExponent)
	t.Run("ToNormalizedString should return a string to 2 decimals", testToNormalizedString)
	t.Run("Truncate should truncate a Decimal", testTruncate)
	t.Run("Zero should have 0 for mantissa and exponent", testZero)
}

func testAdd(t *testing.T) {
	a := decimal.Decimal{Mantissa: 12, Exponent: -1}
	b := decimal.Decimal{Mantissa: 34, Exponent: 1}
	test.Equals(t, decimal.Decimal{Mantissa: 3412, Exponent: -1}, a.Add(b))
}

func testCompareEqual(t *testing.T) {
	a := decimal.Decimal{Mantissa: 12, Exponent: -1}
	b := decimal.Decimal{Mantissa: 34, Exponent: 1}
	test.Equals(t, -1, a.Compare(b))
}

func testCompareGreater(t *testing.T) {
	a := decimal.Decimal{Mantissa: 34, Exponent: 1}
	b := decimal.Decimal{Mantissa: 12, Exponent: -1}
	test.Equals(t, 1, a.Compare(b))
}

func testCompareLess(t *testing.T) {
	a := decimal.Decimal{Mantissa: 120, Exponent: 0}
	b := decimal.Decimal{Mantissa: 12, Exponent: 1}
	test.Equals(t, 0, a.Compare(b))
}

func testEqDifferent(t *testing.T) {
	a := decimal.Decimal{Mantissa: 120, Exponent: 1}
	b := decimal.Decimal{Mantissa: 12, Exponent: 1}
	test.False(t, a.Eq(b))
}

func testEqSame(t *testing.T) {
	a := decimal.Decimal{Mantissa: 120, Exponent: 0}
	b := decimal.Decimal{Mantissa: 12, Exponent: 1}
	test.True(t, a.Eq(b))
}

func testLtFalse(t *testing.T) {
	a := decimal.Decimal{Mantissa: 12, Exponent: 1}
	b := decimal.Decimal{Mantissa: 11, Exponent: 1}
	test.False(t, a.Lt(b))
}

func testLtTrue(t *testing.T) {
	a := decimal.Decimal{Mantissa: 11, Exponent: 1}
	b := decimal.Decimal{Mantissa: 12, Exponent: 1}
	test.True(t, a.Lt(b))
}

func testNegate(t *testing.T) {
	test.Equals(t, decimal.Decimal{Mantissa: -11, Exponent: 1}, (decimal.Decimal{Mantissa: 11, Exponent: 1}).Negate())
}

func testNormalizeHundredths(t *testing.T) {
	test.Equals(t, decimal.Decimal{Mantissa: 11000, Exponent: -2}, (decimal.Decimal{Mantissa: 11, Exponent: 1}).NormalizeHundredths())
}

func testNew(t *testing.T) {
	test.Equals(t, decimal.Decimal{Mantissa: 1, Exponent: 2}, decimal.New(1, 2))
}

func testNewFromInt(t *testing.T) {
	test.Equals(t, decimal.Decimal{Mantissa: 1, Exponent: 0}, decimal.NewFromInt(1))
}

func testNewFromString(t *testing.T) {
	d, err := decimal.NewFromString("1.2")
	test.OK(t, err)
	test.Equals(t, decimal.Decimal{Mantissa: 12, Exponent: -1}, d)
}

func testNewFromStringInvalid(t *testing.T) {
	_, err := decimal.NewFromString("xxx")
	test.ExpectedError(t, "strconv.Atoi: parsing \"xxx\": invalid syntax", err)
}

func testSub(t *testing.T) {
	a := decimal.Decimal{Mantissa: 12, Exponent: 1}
	b := decimal.Decimal{Mantissa: 34, Exponent: -1}
	test.Equals(t, decimal.Decimal{Mantissa: 1166, Exponent: -1}, a.Sub(b))
}

func testToCommonExponent(t *testing.T) {
	a := decimal.Decimal{Mantissa: 12, Exponent: 1}
	b := decimal.Decimal{Mantissa: 34, Exponent: -1}
	a, b = a.ToCommonExponent(b)
	test.Equals(t, decimal.Decimal{Mantissa: 1200, Exponent: -1}, a)
	test.Equals(t, decimal.Decimal{Mantissa: 34, Exponent: -1}, b)
}

func testToNormalizedString(t *testing.T) {
	test.Equals(t, "1234.50", (decimal.Decimal{Mantissa: 12345, Exponent: -1}).ToNormalizedString())
}

func testTruncate(t *testing.T) {
	test.Equals(t, decimal.Decimal{Mantissa: 1234, Exponent: -2}, (decimal.Decimal{Mantissa: 12345, Exponent: -3}).Truncate(2))
}

func testZero(t *testing.T) {
	test.True(t, decimal.Zero.Mantissa == 0)
	test.True(t, decimal.Zero.Exponent == 0)
}
