/*
 * Copyright © 2017, G.Ralph Kuntz, MD.
 *
 * Licensed under the Apache License, Version 2.0(the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIC
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/exec"
	"os/user"
	"path"
	"runtime"
	"strings"
	"time"

	"bitbucket.org/grkuntzmd/checks-and-balances/server"
	"bitbucket.org/grkuntzmd/checks-and-balances/server/controllers"
	"bitbucket.org/grkuntzmd/checks-and-balances/server/db"
	"github.com/gorilla/mux"
	"github.com/tebeka/atexit"
	lumberjack "gopkg.in/natefinch/lumberjack.v2"
)

var (
	buildInfo string
	info      = server.NewInfo()

	databaseDirectory string
)

func init() {
	user, err := user.Current()
	if err != nil {
		fmt.Fprintf(os.Stderr, "unable to get current user: %s\n", err)
		atexit.Exit(1)
	}

	flag.StringVar(&databaseDirectory, "d", path.Join(user.HomeDir, ".checks-and-balances"),
		"`directory` where the database is stored")
}

func main() {
	if buildInfo != "" {
		parts := strings.Split(buildInfo, "|")
		if len(parts) >= 3 {
			info.BuildStamp = parts[0]
			info.GitHash = parts[1]
			info.Version = parts[2]
		}
	}

	flag.CommandLine.Usage = usage
	flag.Parse()

	if err := os.MkdirAll(databaseDirectory, os.ModeDir|0700); err != nil {
		panic(err)
	}

	logger := &lumberjack.Logger{
		Filename:   path.Join(databaseDirectory, "checks-and-balances.log"),
		MaxSize:    500, // megabytes
		MaxBackups: 3,
		MaxAge:     28, //days
		Compress:   true,
	}
	log.SetOutput(logger)

	repo, err := db.Setup(databaseDirectory)
	if err != nil {
		log.Printf("error opening database: %s", err)
		atexit.Exit(1)
	}

	server := &http.Server{}
	router := mux.NewRouter().StrictSlash(true)
	server.Handler = controllers.Routes(server, router, repo, info)

	// https://gist.github.com/xcsrz/538e291d12be6ee9a8c7
	listener, err := net.Listen("tcp", "127.0.0.1:0")
	if err != nil {
		panic(err)
	}
	log.Printf("listening on %s", listener.Addr().String())

	if err = open("http://" + listener.Addr().String() + "/index.html"); err != nil {
		fmt.Fprintln(os.Stderr, "unable to open default browser", err)
		atexit.Exit(1)
	}

	waitForClientShutdown(server, logger)

	if err = server.Serve(listener); err != http.ErrServerClosed {
		panic(err)
	}
}

// http://stackoverflow.com/a/39324149/96233
func open(url string) error {
	var cmd string
	var args []string

	switch runtime.GOOS {
	case "windows":
		cmd = "cmd"
		args = []string{"/c", "start"}
	case "darwin":
		cmd = "open"
	default: // "linux", "freebsd", "openbsd", "netbsd"
		cmd = "xdg-open"
	}
	args = append(args, url)
	return exec.Command(cmd, args...).Start()
}

func usage() {
	fmt.Fprintf(os.Stderr, "Usage: %s\n", path.Base(os.Args[0]))
	fmt.Fprintf(os.Stderr, "buildStamp: %s, gitHash: %s, version: %s\n", info.BuildStamp, info.GitHash, info.Version)
	flag.PrintDefaults()
}

func waitForClientShutdown(server *http.Server, logger *lumberjack.Logger) {
	go func() {
		for {
			if time.Since(controllers.LastPing).Seconds() > 30 {
				log.Printf("Shutting down server. Time since last update was %0.1f seconds", time.Since(controllers.LastPing).Seconds())
				if err := server.Shutdown(context.Background()); err != nil {
					log.Print(err)
				}
				if err := logger.Rotate(); err != nil {
					log.Print(err)
				}
				atexit.Exit(0)
			}
			time.Sleep(10 * time.Second)
		}
	}()
}
