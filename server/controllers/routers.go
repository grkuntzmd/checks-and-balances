/*
 * Copyright © 2017, G.Ralph Kuntz, MD.
 *
 * Licensed under the Apache License, Version 2.0(the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIC
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package controllers

import (
	"bytes"
	"log"
	"net/http"
	"time"

	"bitbucket.org/grkuntzmd/checks-and-balances/server"
	"bitbucket.org/grkuntzmd/checks-and-balances/server/models"
	assetfs "github.com/elazarl/go-bindata-assetfs"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

type middleware func(http.Handler) http.Handler

// rootDir is used by go-bindata.
var (
	// Time of the last ping call from the client.
	LastPing = time.Now()
	rootDir  = "./"
)

// Routes creates and returns a handler for all server routes.
func Routes(server *http.Server, router *mux.Router, rep models.Repository, info server.Info) http.Handler {
	router.HandleFunc("/", func(_ http.ResponseWriter, _ *http.Request) {
		LastPing = time.Now()
	}).Methods(http.MethodOptions)

	addAPIRoutes(router, rep)
	addPublicRoute(router)

	return adapt(router, timerHandler,
		handlers.RecoveryHandler(handlers.PrintRecoveryStack(true)),
		handlers.CompressHandler)
}

func adapt(h http.Handler, middlewares ...middleware) http.Handler {
	for _, m := range middlewares {
		h = m(h)
	}
	return h
}

func addCacheControl(h http.Handler) http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		res.Header().Set("Cache-Control", "public; max-age=2592000")
		h.ServeHTTP(res, req)
	}
}

func addAPIRoutes(router *mux.Router, rep models.Repository) {
	router.HandleFunc("/balances/{year:\\d{4}}", getBalancesForYear(rep)).Methods(http.MethodGet)

	router.HandleFunc("/transactions/{uuid}", deleteTransaction(rep)).Methods(http.MethodDelete)
	router.HandleFunc("/transactions/{year:\\d{4}}", getTransactions(rep)).Methods(http.MethodGet)
	router.HandleFunc("/transactions/date-of-first", getDateOfFirstTransaction(rep)).Methods(http.MethodGet)
	router.HandleFunc("/transactions", putTransaction(rep)).Methods(http.MethodPut)
}

func addPublicRoute(router *mux.Router) {
	router.PathPrefix("/").Handler(http.StripPrefix("/", addCacheControl(http.FileServer(&assetfs.AssetFS{
		Asset:     Asset,
		AssetDir:  AssetDir,
		AssetInfo: AssetInfo,
		Prefix:    "dist",
	}))))
}

func timerHandler(h http.Handler) http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		start := time.Now()
		var b bytes.Buffer

		handlers.LoggingHandler(&b, h).ServeHTTP(res, req)

		prefix := bytes.TrimRight(b.Bytes(), "\n")
		log.Printf("%s completed in %v", string(prefix), time.Since(start))
	})
}
