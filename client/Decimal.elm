-- From https://github.com/javcasas/elm-decimal


module Decimal
    exposing
        ( Decimal(..)
        , add
        , eq
        , fromString
        , lt
        , negate
        , normalize
        , sub
        , toNormalizedString
        , toString
        , truncate
        , zero
        )

import Maybe exposing (Maybe, withDefault)
import String
    exposing
        ( dropLeft
        , dropRight
        , join
        , left
        , length
        , repeat
        , right
        , split
        , toInt
        , toLower
        )


type alias Mantissa =
    Int


type alias Exponent =
    Int


type Decimal
    = Decimal Mantissa Exponent


fromInt : Int -> Decimal
fromInt n =
    fromIntWithExponent n 0


fromIntWithExponent : Int -> Int -> Decimal
fromIntWithExponent n e =
    Decimal n e


fromString : String -> Maybe Decimal
fromString s =
    let
        stringIntToDecimal s_ e =
            case toInt s_ of
                Ok n ->
                    Decimal n e |> Just

                Err _ ->
                    Nothing
    in
        case split "." s of
            [ a, b ] ->
                stringIntToDecimal (a ++ b) (-(length b))

            [ a ] ->
                stringIntToDecimal a 0

            _ ->
                Nothing


toString : Decimal -> String
toString (Decimal m e) =
    let
        absM =
            if m >= 0 then
                m
            else
                -m

        s =
            Basics.toString absM

        sign =
            if m >= 0 then
                ""
            else
                "-"

        insertDecimalPeriod : Int -> String -> String
        insertDecimalPeriod pos s =
            let
                extraZeros =
                    pos - length s

                paddedS =
                    if extraZeros >= 0 then
                        repeat (extraZeros + 1) "0" ++ s
                    else
                        s

                before =
                    dropRight pos paddedS

                after =
                    right pos paddedS
            in
                before ++ "." ++ after
    in
        case Basics.compare e 0 of
            EQ ->
                sign ++ s

            GT ->
                sign ++ s ++ repeat e "0"

            LT ->
                sign ++ insertDecimalPeriod (0 - e) s


addDecimals : Int -> Decimal -> Decimal
addDecimals i (Decimal m e) =
    if i == 0 then
        Decimal m e
    else if i > 0 then
        addDecimals (i - 1) (Decimal (m * 10) (e - 1))
    else
        Decimal m e


normalize : Decimal -> Decimal
normalize d =
    let
        (Decimal _ e) =
            d
    in
        addDecimals (2 + e) d


toNormalizedString : Decimal -> String
toNormalizedString d =
    normalize d |> toString


toCommonExponent : ( Decimal, Decimal ) -> ( Decimal, Decimal )
toCommonExponent ( a, b ) =
    let
        toExponent : Exponent -> Decimal -> Decimal
        toExponent e (Decimal md ed) =
            addDecimals (ed - e) (Decimal md ed)

        (Decimal ma ea) =
            a

        (Decimal mb eb) =
            b

        exponent =
            min ea eb
    in
        ( toExponent exponent a, toExponent exponent b )


add : Decimal -> Decimal -> Decimal
add a b =
    let
        ( Decimal ma ea, Decimal mb eb ) =
            toCommonExponent ( a, b )
    in
        Decimal (ma + mb) ea


negate : Decimal -> Decimal
negate (Decimal m e) =
    Decimal (-m) e


sub : Decimal -> Decimal -> Decimal
sub a b =
    add a (negate b)


compare : Decimal -> Decimal -> Order
compare a b =
    let
        ( Decimal ma _, Decimal mb _ ) =
            toCommonExponent ( a, b )
    in
        Basics.compare ma mb


eq : Decimal -> Decimal -> Bool
eq a b =
    case compare a b of
        EQ ->
            True

        _ ->
            False


lt : Decimal -> Decimal -> Bool
lt a b =
    case compare a b of
        LT ->
            True

        _ ->
            False


zero : Decimal
zero =
    fromInt 0


truncate : Int -> Decimal -> Decimal
truncate n d =
    let
        s =
            toString d

        toDecimal : String -> Decimal
        toDecimal s =
            fromString s |> withDefault zero
    in
        case ( split "." s, n >= 0 ) of
            ( [ a ], True ) ->
                String.left n a |> toDecimal

            ( [ a ], False ) ->
                toDecimal a

            ( [ a, b ], True ) ->
                a ++ "." ++ String.left n b |> toDecimal

            ( [ a, b ], False ) ->
                a ++ "." ++ String.left (-n) b |> toDecimal

            _ ->
                zero


round : Int -> Decimal -> Decimal
round n d =
    let
        signAsInt : Decimal -> Int
        signAsInt d =
            case compare d zero of
                LT ->
                    -1

                EQ ->
                    0

                GT ->
                    1

        getDigit : Int -> Decimal -> Int
        getDigit n d =
            let
                s =
                    toString d

                toInt : String -> Int
                toInt d =
                    case d of
                        "0" ->
                            0

                        "1" ->
                            1

                        "2" ->
                            2

                        "3" ->
                            3

                        "4" ->
                            4

                        "5" ->
                            5

                        "6" ->
                            6

                        "7" ->
                            7

                        "8" ->
                            8

                        "9" ->
                            9

                        "" ->
                            0

                        _ ->
                            -1
            in
                case ( split "." s, Basics.compare n 0 ) of
                    ( [ a ], GT ) ->
                        dropRight n a |> right 1 |> toInt

                    ( [ a ], EQ ) ->
                        right 1 a |> toInt

                    ( [ a ], LT ) ->
                        0

                    ( [ a, b ], GT ) ->
                        dropRight n a |> right 1 |> toInt

                    ( [ a, b ], EQ ) ->
                        right 1 a |> toInt

                    ( [ a, b ], LT ) ->
                        dropLeft (-n - 1) b |> left 1 |> toInt

                    _ ->
                        -13

        t =
            truncate n d

        nextDigit =
            getDigit (n - 1) d

        toIncrement =
            fromIntWithExponent (signAsInt d) n
    in
        if nextDigit >= 5 then
            add t toIncrement
        else
            t
