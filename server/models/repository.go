/*
 * Copyright © 2017, G.Ralph Kuntz, MD.
 *
 * Licensed under the Apache License, Version 2.0(the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIC
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package models

import (
	"strconv"
	"time"

	"bitbucket.org/grkuntzmd/checks-and-balances/server/decimal"
)

const (
	// DateFormat is the string format for all date keys in the database.
	DateFormat = "2006-01-02"
)

// CheckNumber is a wrapper for int.
type CheckNumber int

// Repository contains the functions that a database must provide.
type Repository interface {
	ReadBalances(time.Time) (*Balances, error)

	DeleteTransaction([]byte) error
	ReadDateOfFirstTransaction() (time.Time, error)
	ReadTransactions(time.Time) ([]*Transaction, error)
	WriteTransaction(*Transaction) error
}

// Balances represents the balances on the first day of a month, before any transactions.
type Balances struct {
	ActualBalance decimal.Decimal `json:"actualBalance"`
	BankBalance   decimal.Decimal `json:"bankBalance"`
}

// Dates represents an index of dates to transactions.
type Dates []string

// Transaction represents a checkbook transaction.
type Transaction struct {
	UUID        string          `json:"uuid"`
	CheckNumber *CheckNumber    `json:"checkNumber,omitempty"`
	Date        time.Time       `json:"date"`
	Description string          `json:"description"`
	Amount      decimal.Decimal `json:"amount"`
	Reconciled  bool            `json:"reconciled"`
}

// String satisfies the fmt.Stringer interface.
func (c *CheckNumber) String() string {
	return strconv.Itoa(int(*c))
}
