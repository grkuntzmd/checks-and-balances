#!/bin/sh

cd "${0%/*}"

rsrc -manifest checks-and-balances.exe.manifest \
  -ico images/favicon.ico -arch="amd64" \
  -o cmd/checks-and-balances/check-and-balances_windows_amd64.syso