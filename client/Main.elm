port module Main exposing (main)

import Body
import Date exposing (Date, Month(Jan), year)
import Date.Extra exposing (Interval(..), add, fromIsoString, toFormattedString)
import Date.Extra as Extra
import Decimal exposing (fromString, eq, lt, normalize, sub, zero)
import Dom.Scroll exposing (toBottom)
import Footer
import Header
import Html exposing (Html, div, text)
import Html.Attributes exposing (class)
import Http
import List exposing (filter, foldl, head, range, reverse, sortWith)
import List.Extra exposing (find, replaceIf)
import List.Extra as Extra
import Maybe exposing (andThen, withDefault)
import Model exposing (Model, epoch, init)
import Msg exposing (Msg(..))
import Random.Pcg exposing (Seed, initialSeed, step)
import Regex exposing (HowMany(..), Regex, regex)
import Remote exposing (RemoteTransaction)
import RemoteData exposing (RemoteData(..))
import Server
import Status exposing (Status(..))
import String exposing (contains, length, toLower)
import Task exposing (attempt)
import Time exposing (every, inSeconds, second)
import Transaction
    exposing
        ( Balances
        , Transaction
        , TransactionKind(..)
        , TransactionType(..)
        )
import Uuid exposing (Uuid, uuidGenerator)


main : Program Never Model Msg
main =
    Html.program
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    Tuple.second <|
        Debug.log "Main msg, update"
            ( msg
            , case msg of
                CancelEdit ->
                    cancelEdit model

                ConfirmDelete uuid ->
                    ( { model
                        | delete = Just uuid
                        , dialogOpen = Body.confirmID
                      }
                    , Cmd.none
                    )

                DeleteCancel ->
                    ( { model
                        | delete = Nothing
                        , dialogOpen = ""
                      }
                    , Cmd.none
                    )

                DeleteOK ->
                    deleteOK model

                DeleteTransaction transactions response ->
                    case response of
                        Ok _ ->
                            deleteTransaction transactions model

                        Err error ->
                            handleError error model

                Edit uuid ->
                    edit uuid model

                FetchBalances response ->
                    fetchBalances response model

                FetchOldestDate response ->
                    fetchOldestDate response model

                FetchTransactions response ->
                    fetchTransactions response model

                HideDialog ->
                    ( { model
                        | delete = Nothing
                        , dialogOpen = ""
                        , error = Nothing
                      }
                    , Cmd.none
                    )

                InitializeSeeds seed ->
                    ( { model | seed = initialSeed seed }, Cmd.none )

                NoOpDom _ ->
                    ( model, Cmd.none )

                NoOpRemote _ ->
                    ( model, Cmd.none )

                PutTransaction recalcDates seed transactions response ->
                    case response of
                        Ok _ ->
                            putTransaction recalcDates seed transactions model

                        Err error ->
                            handleError error model

                ReceiveDate date ->
                    receiveDate date model

                Reconcile uuid ->
                    reconcile uuid model

                SetFirstMonth value ->
                    setFirstMonth value model

                SetStatus status ->
                    ( recalculate { model | showStatus = status }, Cmd.none )

                Search term ->
                    ( recalculate { model | searchTerm = term }, Cmd.none )

                SetTransactionType transactionType ->
                    ( { model | transactionType = transactionType }, Cmd.none )

                ShowLicense ->
                    ( { model | dialogOpen = Header.licenseID }, Cmd.none )

                Tick time ->
                    let
                        _ =
                            Debug.log "tick" ()
                    in
                        ( model, Server.pingServerCmd )

                TransactionAdd ->
                    transactionAdd model

                TransactionAmount amount ->
                    transactionAmount amount model

                TransactionCheckNumber value ->
                    transactionCheckumber value model

                TransactionDate dateText ->
                    ( { model
                        | transactionDate = fromIsoString dateText
                        , transactionDateRequired = False
                      }
                    , Cmd.none
                    )

                TransactionDescription description ->
                    transactionDescription description model
            )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ dialogClosed (\_ -> HideDialog)
        , every (15 * second) Tick
        ]


view : Model -> Html Msg
view model =
    div [ class "main" ]
        [ Header.view model
        , Body.view model
        , Footer.view model
        ]


port dialogClosed : (() -> msg) -> Sub msg


cancelEdit : Model -> ( Model, Cmd Msg )
cancelEdit model =
    ( { model
        | transactionAmount = Nothing
        , transactionAmountRequired = False
        , transactionCheckNumber = Nothing
        , transactionDate = Just model.currentDate
        , transactionDateRequired = False
        , transactionDescription = ""
        , transactionDescriptionRequired = False
        , transactionType = WithdrawalType
        , transactionUuid = Nothing
      }
    , Cmd.none
    )


checkNumberRegex : Regex
checkNumberRegex =
    regex """\\d{1,5}"""


deleteOK : Model -> ( Model, Cmd Msg )
deleteOK model =
    case model.delete of
        Just uuid ->
            let
                allTransactions =
                    filter (\t -> t.uuid /= uuid)
                        model.allTransactions
            in
                ( { model
                    | delete = Nothing
                    , dialogOpen = ""
                  }
                , Server.deleteTransactionCmd allTransactions uuid
                )

        Nothing ->
            ( model, Cmd.none )


deleteTransaction : List Transaction -> Model -> ( Model, Cmd Msg )
deleteTransaction transactions model =
    ( recalculate { model | allTransactions = transactions }, Cmd.none )


edit : Uuid -> Model -> ( Model, Cmd Msg )
edit uuid model =
    let
        maybeFound =
            Extra.find (\t -> t.uuid == uuid)
                model.allTransactions
    in
        case maybeFound of
            Just item ->
                let
                    ( amount, transactionType ) =
                        case item.kind of
                            Deposit amount ->
                                ( amount, DepositType )

                            Withdrawal amount ->
                                ( amount, WithdrawalType )
                in
                    ( recalculate
                        { model
                            | transactionAmount =
                                Decimal.toString amount |> Just
                            , transactionAmountRequired = False
                            , transactionCheckNumber =
                                item.checkNumber
                            , transactionDate = Just item.date
                            , transactionDateRequired = False
                            , transactionDescription = item.description
                            , transactionType = transactionType
                            , transactionUuid = Just item.uuid
                        }
                    , Cmd.none
                    )

            Nothing ->
                ( model, Cmd.none )


fetchBalances : RemoteData e Balances -> Model -> ( Model, Cmd Msg )
fetchBalances response model =
    case response of
        Success value ->
            let
                firstYear =
                    year model.oldestMonth

                lastYear =
                    year model.currentDate

                cmds =
                    range firstYear lastYear
                        |> List.map (Server.fetchTransactions)
            in
                ( recalculate
                    { model
                        | actualBalance = value.actualBalance
                        , bankBalance = value.bankBalance
                    }
                , Cmd.batch cmds
                )

        _ ->
            ( model, Cmd.none )


fetchOldestDate : RemoteData e String -> Model -> ( Model, Cmd Msg )
fetchOldestDate response model =
    case response of
        Success value ->
            case fromIsoString value of
                Just date ->
                    let
                        oldestMonth =
                            Extra.floor Extra.Month date

                        lastYear =
                            add Year (-1) model.currentDate
                                |> toFormattedString "yyyy"
                    in
                        ( recalculate { model | oldestMonth = oldestMonth }
                        , Server.fetchBalances lastYear
                        )

                Nothing ->
                    ( model, Cmd.none )

        _ ->
            ( model, Cmd.none )


fetchTransactions :
    RemoteData e (List RemoteTransaction)
    -> Model
    -> ( Model, Cmd Msg )
fetchTransactions response model =
    case response of
        Success remote ->
            let
                ( uuid_, seed ) =
                    step uuidGenerator model.seed

                transactions =
                    List.map
                        (\r ->
                            let
                                date =
                                    case fromIsoString r.date of
                                        Just date_ ->
                                            date_

                                        Nothing ->
                                            epoch

                                kind =
                                    if lt r.amount zero then
                                        Decimal.negate r.amount |> Withdrawal
                                    else
                                        Deposit r.amount

                                uuid =
                                    case Uuid.fromString r.uuid of
                                        Just uuid_ ->
                                            uuid_

                                        Nothing ->
                                            uuid_
                            in
                                { actualBalance = zero
                                , bankBalance = zero
                                , checkNumber = r.checkNumber
                                , date = date
                                , description = r.description
                                , kind = kind
                                , reconciled = r.reconciled
                                , uuid = uuid
                                }
                        )
                        remote

                allTransactions =
                    transactions ++ model.allTransactions |> sort
            in
                ( recalculate
                    { model
                        | allTransactions = allTransactions
                        , firstMonth =
                            getFirstMonth
                                (add Month (-3) model.currentDate)
                                allTransactions
                        , seed = seed
                    }
                , Cmd.none
                )

        _ ->
            ( model, Cmd.none )


findTransaction : Uuid -> List Transaction -> Maybe Transaction
findTransaction uuid transactions =
    Extra.find (\t -> t.uuid == uuid) transactions


getFirstMonth : Date -> List Transaction -> Date
getFirstMonth date transactions =
    let
        maybeOldestUnreconciled =
            Extra.find (\t -> not t.reconciled) transactions
    in
        case maybeOldestUnreconciled of
            Just transaction ->
                Extra.floor Extra.Month transaction.date

            Nothing ->
                Extra.floor Extra.Month date


getOldestMonth : Date -> List Transaction -> Date
getOldestMonth date transactions =
    let
        maybeOldest =
            head transactions
    in
        case maybeOldest of
            Just transaction ->
                Extra.floor Extra.Month transaction.date

            Nothing ->
                Extra.floor Extra.Month date


handleError : Http.Error -> Model -> ( Model, Cmd Msg )
handleError error model =
    let
        err =
            case error of
                Http.BadStatus r ->
                    (toString r.status.code) ++ r.body

                _ ->
                    toString error
    in
        ( { model | error = Just err }, Cmd.none )


putTransaction : Bool -> Seed -> List Transaction -> Model -> ( Model, Cmd Msg )
putTransaction recalcDates seed transactions model =
    let
        ( firstMonth, oldestMonth, scroll ) =
            if recalcDates then
                ( getFirstMonth epoch transactions
                , getOldestMonth epoch transactions
                , attempt NoOpDom (toBottom "body")
                )
            else
                ( model.firstMonth
                , model.oldestMonth
                , Cmd.none
                )
    in
        ( recalculate
            { model
                | allTransactions = transactions
                , firstMonth = firstMonth
                , oldestMonth = oldestMonth
                , seed = seed
                , transactionAmount = Nothing
                , transactionAmountRequired = False
                , transactionCheckNumber = Nothing
                , transactionDate = Just model.currentDate
                , transactionDateRequired = False
                , transactionDescription = ""
                , transactionDescriptionRequired = False
                , transactionType = WithdrawalType
                , transactionUuid = Nothing
            }
        , scroll
        )


recalculate : Model -> Model
recalculate model =
    let
        allTransactions =
            sort model.allTransactions
                |> foldl
                    (\t ( ( a, b ), acc ) ->
                        let
                            a_ =
                                case t.kind of
                                    Deposit amount ->
                                        Decimal.add a amount

                                    Withdrawal amount ->
                                        sub a amount

                            b_ =
                                if t.reconciled then
                                    case t.kind of
                                        Deposit amount ->
                                            Decimal.add b amount

                                        Withdrawal amount ->
                                            sub b amount
                                else
                                    b
                        in
                            ( ( a_, b_ )
                            , { t
                                | bankBalance = b_
                                , actualBalance = a_
                              }
                                :: acc
                            )
                    )
                    ( ( model.actualBalance, model.bankBalance ), [] )
                |> Tuple.second
                |> reverse

        displayedTransactions =
            filter
                (\t ->
                    case model.showStatus of
                        Status.All ->
                            True

                        Open ->
                            not t.reconciled

                        Reconciled ->
                            t.reconciled
                )
                allTransactions
                |> filter
                    (\t ->
                        case Extra.compare t.date model.firstMonth of
                            LT ->
                                False

                            _ ->
                                True
                    )
                |> filter
                    (\t ->
                        if length model.searchTerm == 0 then
                            True
                        else
                            let
                                checkNumber =
                                    Maybe.map toString t.checkNumber
                                        |> withDefault ""

                                date =
                                    toFormattedString "dd/MM/yyyy" t.date

                                searchTerm =
                                    toLower model.searchTerm
                            in
                                contains searchTerm checkNumber
                                    || contains searchTerm date
                                    || contains searchTerm
                                        (toLower t.description)
                    )
    in
        { model
            | allTransactions = allTransactions
            , displayedTransactions = displayedTransactions
        }


receiveDate : Date -> Model -> ( Model, Cmd Msg )
receiveDate date model =
    let
        allTransactions =
            sort model.allTransactions
    in
        ( recalculate
            { model
                | allTransactions = allTransactions
                , currentDate = date
                , firstMonth = getFirstMonth date allTransactions
                , oldestMonth = Extra.floor Extra.Month date
                , transactionDate = Just date
            }
        , Cmd.none
        )


reconcile : Uuid -> Model -> ( Model, Cmd Msg )
reconcile uuid model =
    let
        maybeTransaction =
            findTransaction uuid model.allTransactions
                |> Maybe.map (\t -> { t | reconciled = not t.reconciled })
    in
        case maybeTransaction of
            Just transaction ->
                let
                    allTransactions =
                        replaceTransaction uuid
                            (\t -> { t | reconciled = not t.reconciled })
                            model.allTransactions
                in
                    ( model
                    , Server.putTransactionCmd False
                        model.seed
                        allTransactions
                        transaction
                    )

            Nothing ->
                ( model, Cmd.none )


replaceTransaction :
    Uuid
    -> (Transaction -> Transaction)
    -> List Transaction
    -> List Transaction
replaceTransaction uuid replacer transactions =
    findTransaction uuid transactions
        |> Maybe.map
            (\t ->
                replaceIf (\t -> t.uuid == uuid)
                    (replacer t)
                    transactions
            )
        |> withDefault transactions


setFirstMonth : String -> Model -> ( Model, Cmd Msg )
setFirstMonth value model =
    case value ++ "-01" |> fromIsoString of
        Just date ->
            ( recalculate { model | firstMonth = date }
            , attempt NoOpDom (toBottom "body")
            )

        Nothing ->
            ( recalculate model, Cmd.none )


sort : List Transaction -> List Transaction
sort =
    sortWith
        (\a b ->
            case Extra.compare a.date b.date of
                EQ ->
                    case ( a.checkNumber, b.checkNumber ) of
                        ( Just a_, Just b_ ) ->
                            compare a_ b_

                        ( Nothing, Just b_ ) ->
                            LT

                        ( Just a_, Nothing ) ->
                            GT

                        _ ->
                            EQ

                other ->
                    other
        )


transactionAdd : Model -> ( Model, Cmd Msg )
transactionAdd model =
    let
        checkNumber =
            model.transactionCheckNumber

        ( transactionDateRequired, date ) =
            case model.transactionDate of
                Just date ->
                    ( False, date )

                Nothing ->
                    ( True, epoch )

        ( transactionDescriptionRequired, description ) =
            if length model.transactionDescription > 0 then
                ( False, model.transactionDescription )
            else
                ( True, model.transactionDescription )

        ( transactionAmountRequired, kind ) =
            case model.transactionAmount |> andThen Decimal.fromString of
                Just amount ->
                    let
                        normalized =
                            normalize amount

                        truncated =
                            Decimal.truncate 2 normalized
                    in
                        if eq truncated amount then
                            case model.transactionType of
                                DepositType ->
                                    ( False, Deposit amount )

                                WithdrawalType ->
                                    ( False, Withdrawal amount )
                        else
                            ( True, Deposit zero )

                Nothing ->
                    ( True, Deposit zero )

        ( uuid, seed ) =
            case model.transactionUuid of
                Just uuid ->
                    ( uuid, model.seed )

                Nothing ->
                    step uuidGenerator model.seed

        transaction =
            model.transactionUuid
                |> andThen (\u -> findTransaction u model.allTransactions)
                |> Maybe.map
                    (\t ->
                        { t
                            | checkNumber = checkNumber
                            , date = date
                            , description = description
                            , kind = kind
                        }
                    )
                |> withDefault
                    { actualBalance = zero
                    , bankBalance = zero
                    , checkNumber = checkNumber
                    , date = date
                    , description = description
                    , kind = kind
                    , reconciled = False
                    , uuid = uuid
                    }

        allTransactions =
            sort <|
                case model.transactionUuid of
                    Just uuid ->
                        replaceIf (\t -> t.uuid == uuid)
                            transaction
                            model.allTransactions

                    Nothing ->
                        transaction :: model.allTransactions
    in
        if
            transactionDateRequired
                || transactionDescriptionRequired
                || transactionAmountRequired
        then
            ( { model
                | transactionAmountRequired =
                    transactionAmountRequired
                , transactionDateRequired =
                    transactionDateRequired
                , transactionDescriptionRequired =
                    transactionDescriptionRequired
              }
            , Cmd.none
            )
        else
            ( model
            , Server.putTransactionCmd True seed allTransactions transaction
            )


transactionAmount : String -> Model -> ( Model, Cmd Msg )
transactionAmount amount model =
    let
        transactionAmountRequired =
            case fromString amount of
                Just amount_ ->
                    let
                        normalized =
                            normalize amount_

                        truncated =
                            Decimal.truncate 2 normalized
                    in
                        if eq truncated amount_ then
                            False
                        else
                            True

                Nothing ->
                    True
    in
        ( { model
            | transactionAmount = Just amount
            , transactionAmountRequired = transactionAmountRequired
          }
        , Cmd.none
        )


transactionCheckumber : String -> Model -> ( Model, Cmd Msg )
transactionCheckumber value model =
    let
        maybeNumber =
            Regex.find (AtMost 1) checkNumberRegex value
                |> head
                |> Maybe.map .match
                |> Maybe.andThen
                    (String.toInt >> Result.toMaybe)
    in
        ( { model | transactionCheckNumber = maybeNumber }
        , Cmd.none
        )


transactionDescription : String -> Model -> ( Model, Cmd Msg )
transactionDescription description model =
    ( { model
        | transactionDescription = description
        , transactionDescriptionRequired = length description == 0
      }
    , Cmd.none
    )
